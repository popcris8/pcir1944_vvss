package evaluator;

import com.sun.media.sound.InvalidFormatException;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AddDomainTest_I {
    private static final String file = "src/main/intrebari.txt";

    private IntrebariRepository rep;

    @Before
    public void setUp() throws Exception {
        rep = new IntrebariRepository();
    }

    @Test
    public void testCase1() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase2() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        try {
            Intrebare i = new Intrebare("Enunt1?", "Sadfas", "Raspuns 2", "Raspuns corect", "");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Domeniul este vid!");
        }
    }


    @Test
    public void testCase3() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Denunt?", "Ain enunt nue majus)", "Raspuns 2", "Raspuns corect", "eneral");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Prima litera din domeniu nu e majuscula!");
        }
    }


    @Test
    public void testCase4() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("I23231?", "C44Raspuns 1", "C44Raspuns 2", "Raspuns corect", "Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Lungimea domeniului depaseste 30 de caractere!");
        }
    }

    @Test
    public void testCase5() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Denunt?", "Ain enunt nue majus)", "Raspuns 2", "Raspuns corect", "eneral");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Prima litera din domeniu nu e majuscula!");
        }
    }


    @Test
    public void testCase6() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("A0000000000000?", "Raspuns 1Raspupuns 1Raspuns 1", "Raspuns 2", "Raspuns corect", "2eneral");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Prima litera din domeniu nu e majuscula!");
        }
    }

    @Test
    public void testCase7() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enunt?", "AaaaRaspuns 1", "Raspuns 2", "Raspuns corect", "D");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase8() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("I23231?", "C44Raspuns 1", "C44Raspuns 2", "Raspuns corect", "Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Lungimea domeniului depaseste 30 de caractere!");
        }
    }

    @Test
    public void testCase9() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enuntule?", "Raspuns ss12", "Raspuns ss22", "Raspuns corect mereu", "CorLungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!Lungimea domeniului depaseste 30 de caractere!ect");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Lungimea domeniului depaseste 30 de caractere!");
        }
    }
}
