package evaluator;

import com.sun.media.sound.InvalidFormatException;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import static org.junit.Assert.assertTrue;
public class InvalidWBT_II {
    private static final String file = "src/main/intrebari.txt";

    private IntrebariRepository rep;

    @Before
    public void setUp() throws Exception {
        rep = new IntrebariRepository();
        rep.setUpRepo(file);

    }

    @Test
    public void testCaseInvalidWBT() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        testCase1();
        testCase2();
        testCase3();
        testCase4();
        testCase5();
        testCase6();
        testCase7();
        testCase8();
        testCase9();
    }

    @Test
    public void testCase1() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
           rep.setUpRepo(file);
            Intrebare i = new Intrebare("Corect??", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Enunt1?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i5 = new Intrebare("Enunt2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Enunt3?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            rep.getIntrebari();
            rep.pickRandomIntrebare();
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase2() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Corect2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i6, file);
            rep.getIntrebari();
            rep.pickRandomIntrebare();
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }


    @Test
    public void testCase3() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Este??", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i6 = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i6, file);
            rep.getIntrebari();
            rep.pickRandomIntrebare();
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }


    @Test
    public void testCase4() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i6, file);
            rep.getIntrebari();
            rep.pickRandomIntrebare();
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase5() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("EnuntCorect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i2 = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i3 = new Intrebare("Enunt2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i6, file);
            rep.getIntrebari();
            rep.pickRandomIntrebare();
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }


    @Test
    public void testCase6() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Este?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i3 = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i4 = new Intrebare("Este2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i5 = new Intrebare("Enunt2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("EnuntCorect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i6, file);
            rep.getIntrebari();
            rep.pickRandomIntrebare();
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase7() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);


            Intrebare i = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Este?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i3 = new Intrebare("Este2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i4 = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i5 = new Intrebare("Corect2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("EnuntCorect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            rep.getIntrebari();
            rep.pickRandomIntrebare();
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase8() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Corect2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i3 = new Intrebare("Corect32?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i4 = new Intrebare("Corect332?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i5 = new Intrebare("Corect23?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Este?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            rep.getIntrebari();
            rep.pickRandomIntrebare();
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase9() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Enunt2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i3 = new Intrebare("Este?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i4 = new Intrebare("EnunCorectt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i5 = new Intrebare("Este3?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i6 = new Intrebare("Enunt3?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
            rep.getIntrebari();
            rep.pickRandomIntrebare();
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }
}
