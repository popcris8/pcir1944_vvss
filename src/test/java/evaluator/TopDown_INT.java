package evaluator;

import com.sun.media.sound.InvalidFormatException;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TopDown_INT {
    private static final String file = "src/main/intrebari.txt";

    private IntrebariRepository rep;

    @Before
    public void setUp() throws Exception {
        rep = new IntrebariRepository();
        rep.setUpRepo(file);

    }

    /**
     * Test case for Top Down
     * @throws InputValidationFailedException
     * @throws IOException
     * @throws DuplicateIntrebareException
     */
    @Test
    public void testCaseTopDown() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        testcaseA();
        testCaseIntegratedAB();
        testCaseIntegratedPABC();
    }


    /**
     * Test case unitar pentru A
     * @throws InputValidationFailedException
     * @throws IOException
     * @throws DuplicateIntrebareException
     */
    @Test
    public void testcaseA() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        rep.setUpRepo(file);
        /*****************************/
        /** Test for iteration i **/
            /*Test Case 1*/
        try {
            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertTrue(e.getMessage(), false);
        }
    }


    /**
     * Test case de integrare B (se testeaza A si B)
     * @throws DuplicateIntrebareException
     * @throws InputValidationFailedException
     * @throws IOException
     */
    @Test
    public void testCaseIntegratedAB() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);
            /*****************************/
            /** Test for iteration i **/
            /*Test Case 1*/
            try {
                Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
            } catch (InvalidFormatException e) {
                assertTrue(e.getMessage(), false);
            }
            rep.setUpRepo(file);
            /*Test Case 2*/
            try {
                Intrebare i = new Intrebare("", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
            } catch (InvalidFormatException e) {
                assertEquals(e.getMessage(), "Enuntul este vid!");
            }
            rep.setUpRepo(file);
            /*Test Case 3*/
            try {
                Intrebare i = new Intrebare("Enunt", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
            } catch (InvalidFormatException e) {
                assertEquals(e.getMessage(), "Ultimul caracter din enunt nu e '?'!");
            }
            rep.setUpRepo(file);
            /*Test Case 4*/
            try {
                Intrebare i = new Intrebare("enunt", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
            } catch (InvalidFormatException e) {
                assertEquals(e.getMessage(), "Prima litera din enunt nu e majuscula!");
            }
            /*****************************/
            /** Test for iteration ii **/
            /*Test Case 1*/
            try {
                rep.setUpRepo(file);
                Intrebare i = new Intrebare("Corect??", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i5 = new Intrebare("Enunt2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
                rep.addIntrebare(i5, file);
                rep.getIntrebari();
                rep.pickRandomIntrebare();
            } catch (InvalidFormatException e) {
                System.out.println(" =====================================" + e.getMessage());
                assertTrue(e.getMessage(), false);
            }
            /*Test Case 2*/
            try {
                rep.setUpRepo(file);

                Intrebare i = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i2 = new Intrebare("Corect2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i6 = new Intrebare("Enunnnnt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
                rep.addIntrebare(i2, file);
                rep.addIntrebare(i6, file);
                rep.getIntrebari();
                rep.pickRandomIntrebare();
            } catch (InvalidFormatException e) {
                System.out.println(" =====================================" + e.getMessage());
                assertTrue(e.getMessage(), false);
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }


    /**
     * Test case de integrare C (se testeaza A,B si C) -- PABC --
     * @throws DuplicateIntrebareException
     * @throws InputValidationFailedException
     * @throws IOException
     */
    @Test
    public void testCaseIntegratedPABC() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);
            /*****************************/
            /** Test for iteration i **/
            /*Test Case 1*/
            try {
                Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
            } catch (InvalidFormatException e) {
                assertTrue(e.getMessage(), false);
            }
            rep.setUpRepo(file);
            /*Test Case 2*/
            try {
                Intrebare i = new Intrebare("", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
            } catch (InvalidFormatException e) {
                assertEquals(e.getMessage(), "Enuntul este vid!");
            }
            rep.setUpRepo(file);
            /*Test Case 3*/
            try {
                Intrebare i = new Intrebare("Enunt", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
            } catch (InvalidFormatException e) {
                assertEquals(e.getMessage(), "Ultimul caracter din enunt nu e '?'!");
            }
            rep.setUpRepo(file);
            /*Test Case 4*/
            try {
                Intrebare i = new Intrebare("enunt", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
            } catch (InvalidFormatException e) {
                assertEquals(e.getMessage(), "Prima litera din enunt nu e majuscula!");
            }
            /*****************************/
            /** Test for iteration ii **/
            /*Test Case 1*/
            try {
                rep.setUpRepo(file);
                Intrebare i = new Intrebare("Corect??", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i5 = new Intrebare("Enunt2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
                rep.addIntrebare(i5, file);
                rep.getIntrebari();
                rep.pickRandomIntrebare();
            } catch (InvalidFormatException e) {
                System.out.println(" =====================================" + e.getMessage());
                assertTrue(e.getMessage(), false);
            }
            /*Test Case 2*/
            try {
                rep.setUpRepo(file);

                Intrebare i = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i2 = new Intrebare("Corect2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i6 = new Intrebare("Enunnnnt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
                rep.addIntrebare(i2, file);
                rep.addIntrebare(i6, file);
                rep.getIntrebari();
                rep.pickRandomIntrebare();
            } catch (InvalidFormatException e) {
                System.out.println(" =====================================" + e.getMessage());
                assertTrue(e.getMessage(), false);
            }
            /*****************************/
            /** Test for iteration iii **/
            rep.setUpRepo(file);
            try {
                rep.setUpRepo(file);

                Intrebare i = new Intrebare("Coresssct??", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i2 = new Intrebare("Enusant2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i3 = new Intrebare("Ensasunt3?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i4 = new Intrebare("Enasasun2t?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i5 = new Intrebare("Enuasasnt23?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                Intrebare i6 = new Intrebare("Enuasasnt233?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
                rep.addIntrebare(i, file);
                rep.addIntrebare(i2, file);
                rep.addIntrebare(i3, file);
                rep.addIntrebare(i4, file);
                rep.addIntrebare(i5, file);
                rep.addIntrebare(i6, file);
                Statistica statistica = new Statistica();
                for(String domeniu : rep.getDistinctDomains()){
                    statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
                }
                /*****************************/
            } catch (InvalidFormatException e) {
                System.out.println(" =====================================" + e.getMessage());
                assertTrue(e.getMessage(), false);
            }
            Statistica statistica = new Statistica();
            for (String domeniu : rep.getDistinctDomains()) {
                statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }
}
