package evaluator;

import static org.junit.Assert.*;

import com.sun.media.sound.InvalidFormatException;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class AddQuestionTest_I {
    private static final String file = "src/main/intrebari.txt";

    private IntrebariRepository rep;

    @Before
    public void setUp() throws Exception {
        rep = new IntrebariRepository();
    }

    @Test
    public void testCase1() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase2() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        try {
            Intrebare i = new Intrebare("", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Enuntul este vid!");
        }
    }


    @Test
    public void testCase3() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enunt", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Ultimul caracter din enunt nu e '?'!");
        }
    }


    @Test
    public void testCase4() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("enunt", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Prima litera din enunt nu e majuscula!");
        }
    }

    @Test
    public void testCase5() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("A0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Lungimea enuntului depaseste 40 de caractere!");
        }
    }


    @Test
    public void testCase6() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("A0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Lungimea enuntului depaseste 40 de caractere!");
        }
    }

    @Test
    public void testCase7() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase8() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("i23231", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Prima litera din enunt nu e majuscula!");
        }
    }

    @Test
    public void testCase9() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enuntule?", "Raspuns 12", "Raspuns 22", "Raspuns corect mereu", "Corect");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertTrue(e.getMessage(), false);
        }
    }
}
