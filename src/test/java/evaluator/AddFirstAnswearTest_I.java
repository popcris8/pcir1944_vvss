package evaluator;

import com.sun.media.sound.InvalidFormatException;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AddFirstAnswearTest_I {

    private static final String file = "src/main/intrebari.txt";

    private IntrebariRepository rep;

    @Before
    public void setUp() throws Exception {
        rep = new IntrebariRepository();
    }

    @Test
    public void testCase1() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase2() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        try {
            Intrebare i = new Intrebare("Enunt1?", "", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Varianta1 este vida!");
        }
    }


    @Test
    public void testCase3() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Denunt?", "Ain enunt nu e majusin enunt nu e majusin enunt nu e majusin enunt nu e majusin enunt nu e majusin enunt nu e majusin enunt nu e majus)", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Lungimea variantei1 depaseste 50 de caractere!");
        }
    }


    @Test
    public void testCase4() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enunt?", "aaaaRaspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Prima litera din varianta1 nu e majuscula!");
        }
    }

    @Test
    public void testCase5() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Denunt?", "Ain enunt nu e majusin enunt nu e majusin enunt nu e majusin enunt nu e majusin enunt nu e majusin enunt nu e majusin enunt nu e majus)", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Lungimea variantei1 depaseste 50 de caractere!");
        }
    }


    @Test
    public void testCase6() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("A0000000000000?", "Raspuns 1Raspuns 1Raspuns 1Raspuns 1Raspuns 1Raspuns 1Raspuns 1Raspuns 1Raspuns 1Raspuns 1Raspuns 1Raspuns 1Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Lungimea variantei1 depaseste 50 de caractere!");
        }
    }

    @Test
    public void testCase7() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enunt?", "aaaaRaspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Prima litera din varianta1 nu e majuscula!");
        }
    }

    @Test
    public void testCase8() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("I23231?", "44Raspuns 1", "44Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertEquals(e.getMessage(), "Prima litera din varianta1 nu e majuscula!");
        }
    }

    @Test
    public void testCase9() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            Intrebare i = new Intrebare("Enuntule?", "Raspuns ss12", "Raspuns ss22", "Raspuns corect mereu", "Corect");
            rep.addIntrebare(i, file);
        } catch (InvalidFormatException e) {
            assertTrue(e.getMessage(), false);
        }
    }
}



