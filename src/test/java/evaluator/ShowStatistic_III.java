package evaluator;

import com.sun.media.sound.InvalidFormatException;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;

import static org.junit.Assert.assertTrue;


public class ShowStatistic_III {
    private static final String file = "src/main/intrebari.txt";

    private IntrebariRepository rep;

    @Before
    public void setUp() throws Exception {
        rep = new IntrebariRepository();
        rep.setUpRepo(file);

    }

    @Test
    public void testCase1() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Corect??", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Enunt2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i3 = new Intrebare("Enunt3?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i4 = new Intrebare("Enun2t?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i5 = new Intrebare("Enunt23?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Enunt233?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i3, file);
            rep.addIntrebare(i4, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            Statistica statistica = new Statistica();
            for(String domeniu : rep.getDistinctDomains()){
                statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase2() throws InputValidationFailedException, IOException, DuplicateIntrebareException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Corect22?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i3 = new Intrebare("Enunt2?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i4 = new Intrebare("Enunt3?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i5 = new Intrebare("Enunt4?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Enunt5?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i3, file);
            rep.addIntrebare(i4, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            Statistica statistica = new Statistica();
            for(String domeniu : rep.getDistinctDomains()){
                statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }


    @Test
    public void testCase3() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Este??", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i3 = new Intrebare("Corec2t?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i4 = new Intrebare("Enun2t?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i5 = new Intrebare("Est4e?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i3, file);
            rep.addIntrebare(i4, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            Statistica statistica = new Statistica();
            for(String domeniu : rep.getDistinctDomains()){
                statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }


    @Test
    public void testCase4() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Enun3t?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Este?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i3 = new Intrebare("EnuntCorect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i4 = new Intrebare("Es2te?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i5 = new Intrebare("Enunt1Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Cor3ect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i3, file);
            rep.addIntrebare(i4, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            Statistica statistica = new Statistica();
            for(String domeniu : rep.getDistinctDomains()){
                statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase5() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("EnuntC3orect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i2 = new Intrebare("Core1ct?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i3 = new Intrebare("Enun1t?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i4 = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i5 = new Intrebare("Est2e?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i6 = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i3, file);
            rep.addIntrebare(i4, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            Statistica statistica = new Statistica();
            for(String domeniu : rep.getDistinctDomains()){
                statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }


    @Test
    public void testCase6() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("En1unt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Es3te?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i3 = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i4 = new Intrebare("Es2te?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i5 = new Intrebare("En2unt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Enun3tCorect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i3, file);
            rep.addIntrebare(i4, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            Statistica statistica = new Statistica();
            for(String domeniu : rep.getDistinctDomains()){
                statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase7() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Corect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Est4e?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i3 = new Intrebare("Es3te?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i4 = new Intrebare("Co2rect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i5 = new Intrebare("Cor1ect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("EnuntC4orect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i3, file);
            rep.addIntrebare(i4, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            Statistica statistica = new Statistica();
            for(String domeniu : rep.getDistinctDomains()){
                statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase8() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Co1rect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i3 = new Intrebare("Co12rect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i4 = new Intrebare("Co3rect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i5 = new Intrebare("Cor4ect?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i6 = new Intrebare("Este?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i3, file);
            rep.addIntrebare(i4, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            Statistica statistica = new Statistica();
            for(String domeniu : rep.getDistinctDomains()){
                statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }

    @Test
    public void testCase9() throws DuplicateIntrebareException, InputValidationFailedException, IOException {
        try {
            rep.setUpRepo(file);

            Intrebare i = new Intrebare("Enunt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i2 = new Intrebare("Enun1t?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i3 = new Intrebare("Es2te?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            Intrebare i4 = new Intrebare("Enu4nCorectt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i5 = new Intrebare("E34ste?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "Orice");
            Intrebare i6 = new Intrebare("En5unt?", "Raspuns 1", "Raspuns 2", "Raspuns corect", "General");
            rep.addIntrebare(i, file);
            rep.addIntrebare(i2, file);
            rep.addIntrebare(i3, file);
            rep.addIntrebare(i4, file);
            rep.addIntrebare(i5, file);
            rep.addIntrebare(i6, file);
            Statistica statistica = new Statistica();
            for(String domeniu : rep.getDistinctDomains()){
                statistica.add(domeniu, rep.getNumberOfIntrebariByDomain(domeniu));
            }
        } catch (InvalidFormatException e) {
            System.out.println(" =====================================" + e.getMessage());
            assertTrue(e.getMessage(), false);
        }
    }
}
