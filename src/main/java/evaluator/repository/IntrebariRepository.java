package evaluator.repository;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;


import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.util.InputValidation;

public class IntrebariRepository {
	private List<Intrebare> intrebari;
	private InputValidation inputValidation;
	public IntrebariRepository() {
		setIntrebari(new LinkedList<Intrebare>());

		inputValidation = new InputValidation();
	}

	public void setUpRepo(String file) throws FileNotFoundException {
		PrintWriter writer = new PrintWriter(file);
		writer.print("");
		writer.close();
	}
	public void addIntrebare(Intrebare entity,String file) throws DuplicateIntrebareException, InputValidationFailedException, IOException {
		if(exists(entity))
			throw new DuplicateIntrebareException("Intrebarea deja exista!");

		inputValidation.validateEnunt(entity.getEnunt());
		inputValidation.validateVarianta1(entity.getVarianta1());
		inputValidation.validateVarianta2(entity.getVarianta2());
		inputValidation.validateVariantaCorecta(entity.getVariantaCorecta());
		inputValidation.validateDomeniu(entity.getDomeniu());

		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
		String s = entity.getEnunt()+"\n" + entity.getVarianta1() + "\n" + entity.getVarianta2() + " \n" + entity.getVariantaCorecta() + "\n" + entity.getDomeniu() + "\n" + "##\n";
		out.println(s);
		out.close();

		intrebari.add(entity);
	}
	
	public boolean exists(Intrebare entity){
		for(Intrebare intrebare : intrebari)
			if(intrebare.equals(entity))
				return true;
		return false;
	}
	
	public Intrebare pickRandomIntrebare(){
		Random random = new Random();
		return intrebari.get(random.nextInt(intrebari.size()));
	}
	
	public int getNumberOfDistinctDomains(){
		return getDistinctDomains().size();
		
	}
	
	public Set<String> getDistinctDomains(){
		Set<String> domains = new TreeSet<String>();
		for(Intrebare intrebare : intrebari) {
			if(!domains.contains(intrebare.getDomeniu())){
				domains.add(intrebare.getDomeniu());
			}
		}
		return domains;
	}
	
	public List<Intrebare> getIntrebariByDomain(String domain){
		List<Intrebare> intrebariByDomain = new ArrayList<Intrebare>();
		for(Intrebare intrebare : intrebari){
			if(intrebare.getDomeniu().equals(domain)){
				intrebariByDomain.add(intrebare);
			}
		}
		
		return intrebariByDomain;
	}
	
	public int getNumberOfIntrebariByDomain(String domain){
		return getIntrebariByDomain(domain).size();
	}
	
	public List<Intrebare> loadIntrebariFromFile(String f) throws IOException {
		List<Intrebare> intrebari = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line = null;
		List<String> intrebareAux;
		Intrebare intrebare;
		
		try{
			line = br.readLine();
			while(line != null){
				intrebareAux = new ArrayList<String>();
				intrebareAux.add(line);
				while(!line.equals("##")){
					intrebareAux.add(line);
					line = br.readLine();
				}
				intrebare = new Intrebare();
				intrebare.setEnunt(intrebareAux.get(0));
				intrebare.setVarianta1(intrebareAux.get(2));
				intrebare.setVarianta2(intrebareAux.get(3));
				intrebare.setVariantaCorecta(intrebareAux.get(4));
				intrebare.setDomeniu(intrebareAux.get(5));
				intrebari.add(intrebare);
//				intrebareAux.forEach(e-> System.out.print(e.toString() + "         "));
				line = br.readLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				br.close();
			} catch (IOException e) {
				// TODO: handle exception
			}
		}
		
		return intrebari;
	}
	
	public List<Intrebare> getIntrebari() {
		return intrebari;
	}

	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}
	
}
