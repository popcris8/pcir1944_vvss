package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Test;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_RED = "\u001B[31m";
	private static final String file = "src/main/intrebari.txt";
	
	public static void main(String[] args) throws IOException, InputValidationFailedException, DuplicateIntrebareException, NotAbleToCreateTestException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController();
		appController.loadIntrebariFromFile(file);

		boolean activ = true;
		String optiune = null;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");

			System.out.println("Introduceti optiunea dorita:");
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
				System.out.println(ANSI_GREEN + "Introduceti intrebarea:" +ANSI_RESET);
				String enuntIntrebare = console.readLine();
				System.out.println("Introduceti varianta 1:");
				String varianta1 = console.readLine();
				System.out.println("Introduceti varianta 2:");
				String varianta2 = console.readLine();
				System.out.println("Introduceti varianta corecta:");
				String variantaCorecta = console.readLine();
				System.out.println("Introduceti domeniul:");
				String domeniu = console.readLine();
				Intrebare intrebare = new Intrebare(enuntIntrebare,varianta1,varianta2,variantaCorecta,domeniu);
				appController.addNewIntrebare(intrebare,file);
				break;
			case "2" :
				System.out.println("Creare test!");
				Test test = appController.createNewTest();
				printTest(test);
				break;
			case "3" :
				appController.loadIntrebariFromFile(file);
				Statistica statistica;
				try {
					statistica = appController.getStatistica();
					System.out.println(ANSI_GREEN  + statistica + ANSI_RESET);
				} catch (NotAbleToCreateStatisticsException e) {
					//
				}
				
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

	public static void printTest(Test test) throws IOException {
		Integer scor = 0;
		Random rn = new Random();
		Integer n = 3;
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		for (Intrebare intrebare : test.getIntrebari()) {
			Integer  i = rn.nextInt() % n;
			if(i<0)
				i = i * -1;

			if(i == 2){
				System.out.println(ANSI_GREEN +  intrebare.getEnunt() + ANSI_RESET +"\n" +"1) "+ intrebare.getVarianta1() + "\n" +"2) "+intrebare.getVarianta2() + "\n" + "3) "+intrebare.getVariantaCorecta());
			}else if(i==1){
				System.out.println(ANSI_GREEN +  intrebare.getEnunt() + ANSI_RESET +"\n" +"1) " +intrebare.getVarianta2() + "\n" +"2) "+intrebare.getVariantaCorecta() + "\n" +"3) "+ intrebare.getVarianta1());
			}else{
				System.out.println(ANSI_GREEN +  intrebare.getEnunt() + ANSI_RESET +"\n" +"1) "+ intrebare.getVariantaCorecta() + "\n" +"2) "+intrebare.getVarianta1() + "\n" +"3) "+ intrebare.getVarianta2());
			}
			System.out.println(ANSI_CYAN + "Introduceti raspunsul:" + ANSI_RESET);
			String answer = console.readLine();
			while(!answer.equals("1") && !answer.equals("2") && !answer.equals("3")){
				System.out.println(ANSI_RED + "Introduceti raspunsul (Acesta trebuie sa fie 1, 2 sau 3):" + ANSI_RESET);
				answer = console.readLine();
			}

			String correctAnswer = String.valueOf(i +1);

			if(correctAnswer.contains(answer))
				scor++;

			System.out.println();
		}
		System.out.println(ANSI_YELLOW + "Ati raspuns corect la " + scor + " intrebari" + ANSI_RESET);

	}

}
