package evaluator.util;


import com.sun.media.sound.InvalidFormatException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;

public class InputValidation {


	public  static void validateEnunt(String enunt) throws InputValidationFailedException, InvalidFormatException {
		enunt = enunt.trim();
		if(enunt.equals(""))
			throw new InvalidFormatException("Enuntul este vid!");
		if(!Character.isUpperCase(enunt.charAt(0)))
			throw new InvalidFormatException("Prima litera din enunt nu e majuscula!");
		if(!String.valueOf(enunt.charAt(enunt.length()-1)).equals("?"))
			throw new InvalidFormatException("Ultimul caracter din enunt nu e '?'!");
		if(enunt.length() > 40)
			throw new InvalidFormatException("Lungimea enuntului depaseste 40 de caractere!");
		
	}
	
	public static void validateVarianta1(String varianta1) throws InputValidationFailedException, InvalidFormatException {
		varianta1 = varianta1.trim();
		if(varianta1.equals(""))
			throw new InvalidFormatException("Varianta1 este vida!");
		if(!Character.isUpperCase(varianta1.charAt(0)))
			throw new InvalidFormatException("Prima litera din varianta1 nu e majuscula!");
		if(varianta1.length() > 50)
			throw new InvalidFormatException("Lungimea variantei1 depaseste 50 de caractere!" );
	}
	
	public  static void validateVarianta2(String varianta2) throws InputValidationFailedException{
		
		varianta2 = varianta2.trim();
		
		if(varianta2.equals(""))
			throw new InputValidationFailedException("Varianta2 este vida!");
		if(varianta2.length() > 50)
			throw new InputValidationFailedException("Lungimea variantei2 depaseste 50 de caractere!" );
	}
	
	public  static void validateVarianta3(String varianta3) throws InputValidationFailedException{
		
		varianta3 = varianta3.trim();
		
		if(varianta3.equals(""))
			throw new InputValidationFailedException("Varianta3 este vida!");
		if(varianta3.length() > 50)
			throw new InputValidationFailedException("Lungimea variantei3 depaseste 50 de caractere!" );
	
	}
	
	public  static void validateVariantaCorecta(String variantaCorecta) throws InputValidationFailedException{
		
		variantaCorecta = variantaCorecta.trim();
		
		if(variantaCorecta.equals(""))
			throw new InputValidationFailedException("Varianta corecta este vida");
	}
	
	public static  void validateDomeniu(String domeniu) throws InputValidationFailedException, InvalidFormatException {
		
		domeniu = domeniu.trim();
		
		if(domeniu.equals(""))
			throw new InvalidFormatException("Domeniul este vid!");
		if(!Character.isUpperCase(domeniu.charAt(0)))
			throw new InvalidFormatException("Prima litera din domeniu nu e majuscula!");
		if(domeniu.length() > 30)
			throw new InvalidFormatException("Lungimea domeniului depaseste 30 de caractere!");
		
	}
}
